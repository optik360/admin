FROM ruby:latest
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev qt4-default libqt5webkit5-dev xvfb nodejs npm
RUN apt-get install wget && wget https://github.com/GCorbel/dotfiles/raw/master/development_docker_install.sh -O - | sh

WORKDIR /tmp
RUN wget https://github.com/Medium/phantomjs/releases/download/v2.1.1/phantomjs-2.1.1-linux-x86_64.tar.bz2 && tar -xvf phantomjs-2.1.1-linux-x86_64.tar.bz2 && mv phantomjs-2.1.1-linux-x86_64 /usr/local/share && ln -sf /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin

RUN mkdir /app
WORKDIR /app
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock

RUN bundle config build.nokogiri --use-system-libraries
RUN bundle install --jobs 20
RUN gem install bundler

RUN npm config set registry http://registry.npmjs.org/
RUN npm install -g bower

ADD . /app
CMD rails s -p 5000 -b 0.0.0.0 -e production
