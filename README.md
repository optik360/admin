## Création d'une partie d'admin :

### Description

Le but de l'outil est de pouvoir créer rapidement une nouvelle interface
d'administration. Celui-ci permet également de créer une api qui va pouvoir
être utilisé par les plateformes mobile et d'autres types d'applications. Il
est conçu pour être assez simple à développer avec peu de formation.

Cet outil utilise Angular et Rails. N'ayez craintes, nul besoin de tout
comprendre pour l'utiliser.

Pour voir un exemple vous pouvez voir : https://bitbucket.org/optik360/viensfaireuntour

### Outils utilisés

 - [ng-admin](https://github.com/marmelab/ng-admin) : Outils pour génerer une interface d'admin;
 - [ActiveModelSerializer](https://github.com/rails-api/active_model_serializers) : Connexion entre Rails et Angular;

### Création du projet

Il faut tout d'abord cloner le projet :

    git clone git@bitbucket.org:optik360/admin.git

Pour installer le projet on peut utiliser Docker :

    docker-compose build # pour créer l'image
    docker-compose up # pour lancer l'application

Avec Rails : C'est un peu plus complique pour installer Rails mais ceci devrait fonctionner :

    bundle install
    rake db:setup db:seed
    rails server

### Ajouter un type de données

    rails g resource cat name age:integer birth_date:datetime

Il faut ensuite éditer le fichier `public/admin.js` pour ajouter la partie d'admin. J'ai laisser du code en commentaire pour l'exemple. Normalement ça suffit.

### Hebergement en dev

On peut facilement heberger l'application sur heroku :

    heroku create my-project
    git push heroku master
    heroku run rake db:setup db:seed
    heroku open

On peut se connecter avec "admin@optik360.com" comme login et "12341234" comme mot de passe.
