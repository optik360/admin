require 'json_web_token'

class ApplicationController < ActionController::API
  include CrudConcern

  include ActionController::MimeResponds
  include ActionController::HttpAuthentication::Token::ControllerMethods
  def self.helper_method(*args)
  end
  def self.hide_action(*args)
  end

  include Clearance::Controller
  before_filter :authenticate!

  protected

  def authenticate!
    if claims && user = User.find_by(email: claims[0]['user'])
      @current_user = user
    else
      render head: :ok, status: :unauthorized
    end
  end

  def jwt_token(user)
    JsonWebToken.encode('user' => user.email)
  end

  def claims
    auth_header = request.headers['Authorization'] and
      token = auth_header.split(' ').last and
      JsonWebToken.decode(token)
  rescue
    nil
  end
end
