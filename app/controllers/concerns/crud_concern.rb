module CrudConcern
  extend ActiveSupport::Concern

  def index
    render json: scope.order(order).limit(limit).offset(offset), include: inclusions
  end

  def show
    resource = klass.find(params[:id])
    render json: resource, include: inclusions
  end

  def create
    resource = klass.new(attributes)
    if resource.valid?
      yield(resource) if block_given?
      resource.save!
      render json: resource
    else
      render json: resource.errors.messages, status: :unprocessable_entity
    end
  end

  def update
    resource = klass.find(params[:id])
    if resource.update_attributes(attributes)
      render json: resource
    else
      render json: resource.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    render json: klass.find(params[:id]).destroy
  end

  private

  def attributes
    params.require(resource_symbol).permit!
  end

  def resource_symbol
    resource_name.to_sym
  end

  def resource_name
    klass.name.underscore
  end

  def klass
    self.class.name.gsub('Controller','').singularize.constantize
  end

  def inclusions
    params[:include]
  end

  def scope
    klass.all
  end

  def order
    "#{params[:_sortField]} #{params[:_sortDir]}"
  end

  def limit
    params[:_perPage].to_i
  end

  def offset
    (params[:_page].to_i - 1) * limit
  end
end
