class SessionsController < Clearance::BaseController
  include Clearance::Authentication

  skip_before_filter :authenticate!

  def create
    params[:session][:email] = params[:session][:login]
    admin = authenticate(params)
    sign_in(admin) do
      if admin
        auth_token = jwt_token(admin)
        render json: { token: auth_token }
      else
        render json: { errors: ['Unauthorized'] }, status: :unauthorized
      end
    end
  end

  def destroy
    current_admin_user.reset_access_token!
  end
end
