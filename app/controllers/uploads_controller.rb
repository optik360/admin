class UploadsController < ApplicationController
  def create
    uploader.store!(params[:file])
    render json: {url: uploader.url}
  end

  private

  def uploader
    @uploader ||= ImageUploader.new
  end
end
