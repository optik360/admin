class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :access_token,  :token

  def token
    object.access_token
  end
end
