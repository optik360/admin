Rails.application.routes.draw do
  resources :sessions
  resources :uploads, only: :create
end
