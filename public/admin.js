// declare a new module called 'myApp', and make it require the `ng-admin` module as a dependency
var myApp = angular.module('myApp', ['ng-admin', 'ng-admin.jwt-auth']);
// declare a function to run when the module bootstraps (during the 'config' phase)
myApp.config(['NgAdminConfigurationProvider', 'RestangularProvider', 'ngAdminJWTAuthConfiguratorProvider', function (NgAdminConfigurationProvider, RestangularProvider, ngAdminJWTAuthConfigurator) {
  var nga = NgAdminConfigurationProvider;
  ngAdminJWTAuthConfigurator.setJWTAuthURL('/sessions');

  // create an admin application
  // var admin = nga.application('My First Admin');

  // var picture = nga.entity('pictures');

  // picture.listView().fields([
  //     nga.field('title').isDetailLink(true),
  //     nga.field('description'),
  //     nga.field('url'),
  //     nga.field('address'),
  //     nga.field('lat'),
  //     nga.field('lon'),
  //     nga.field('category_id', 'reference')
  //     .targetEntity(category)
  //     .targetField(nga.field('title'))
  // ]);

  // picture.creationView().fields(picture.listView().fields());
  // picture.showView().fields(picture.listView().fields());
  // picture.editionView().fields(picture.listView().fields());
  // admin.addEntity(picture);

  admin.menu(nga.menu()
      // .addChild(nga.menu(picture).icon('<span class="glyphicon glyphicon-file"></span>'))
      .addChild(nga.menu().title('Logout').icon('<span class="glyphicon glyphicon-file"></span>').link('/logout'))
      );

  nga.configure(admin);
}]);
